CREATE TABLE merch
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) ,
    price VARCHAR(255)
);

INSERT INTO merch(name, price) VALUES('Футболка черная Стоп-кран', '500');
INSERT INTO merch(name, price) VALUES('Футболка белая Стоп-кран', '400');
INSERT INTO merch(name, price) VALUES('Майка Стоп-кран', '350');
INSERT INTO merch(name, price) VALUES('Кружка 15 лет группе Стоп-кран', '300');
INSERT INTO merch(name, price) VALUES('Кружка Стоп-кран', '300');
INSERT INTO merch(name, price) VALUES('Значек Стоп-кран', '350');
INSERT INTO merch(name, price) VALUES('Магнит Стоп-кран', '50');


CREATE TABLE person
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) ,
    email VARCHAR(255),
    password VARCHAR(255)
);

INSERT INTO person(name, email, password) VALUES('pakc', 'pakc@mail.ru', '12345');
INSERT INTO person(name, email, password) VALUES('vipon', 'vipon@mail.ru', '54321');
INSERT INTO person(name, email, password) VALUES('shredder', 'shredder@mail.ru', '11011');
INSERT INTO person(name, email, password) VALUES('toxic paranoia', 'tp@mail.ru', '11001');


CREATE TABLE users_merchlist
(
    person_id bigint,
    merchlist_id bigint,
    UNIQUE (merchlist_id),
    FOREIGN KEY (merchlist_id) REFERENCES public.merch (id) MATCH SIMPLE,
    FOREIGN KEY (person_id) REFERENCES public.person (id) MATCH SIMPLE,
    data DATE,
    quantity int

);

INSERT INTO users_merchlist(person_id, merchlist_id, data, quantity) VALUES('1', '3', '26.09.2022', '2');
INSERT INTO users_merchlist(person_id, merchlist_id, data, quantity) VALUES('3', '5', '10.05.2022', '1');
INSERT INTO users_merchlist(person_id, merchlist_id, data, quantity) VALUES('2', '2', '3.08.2022', '4');
INSERT INTO users_merchlist(person_id, merchlist_id, data, quantity) VALUES('1', '4', '19.06.2022', '1');